# bower-maven-plugin

Fetch [Bower](http://bower.io/) modules in your Maven build.

# Usage

Add the following Maven plugin to your project:

```xml
<build>
    <plugins>
        <plugin>
            <groupId>com.atlassian.maven.plugins</groupId>
            <artifactId>bower-maven-plugin</artifactId>
            <version>0.3</version>
            <executions>
                <execution>
                    <phase>generate-sources</phase>
                    <goals>
                        <goal>fetch-modules</goal>
                    </goals>
                    <configuration>
                        <outputDirectory>${project.basedir}/src/main/resources/bower</outputDirectory>
                        <!--
                        An optional parameter that allows you to use your own Bower registry (useful for internal development).
                        -->
                        <registryCloneUrl>ssh://user@host/your-bower-registry.git</registryCloneUrl>
                        <!-- 
                        This allows transitive dependencies to be excluded. For example if you have your own
                        version of jQuery that you don't want to come from Bower. Another benefit is potentially
                        faster builds.
                        -->
                        <excludes>
                            <excludes>jquery</excludes>
                        </excludes>
                    </configuration>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```