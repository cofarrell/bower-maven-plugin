package com.atlassian.plugin.maven.bower

import org.junit.Test
import org.junit.Assert.assertEquals

class VersionTest {

  @Test def testFilter() = {
    assertEquals(Some("a"), Version("*").filter(Seq("a")))
    assertEquals(Some("1"), Version("1").filter(Seq("1")))
    assertEquals(Some("1.3.3"), Version("~1.3.3").filter(Seq("1.3", "1.3.3")))
    assertEquals(Some("1.3.3.4"), Version("~1.3.3").filter(Seq("1.3", "1.3.3.4")))
    assertEquals(Some("1.0.0"), Version("1").filter(Seq("2.0.0", "1.0.0")))
    assertEquals(Some("2.2.1"), Version(">= 2.2.1").filter(Seq("2.2.0", "2.2.1")))
    assertEquals(Some("2.2.2"), Version(">= 2.2.1").filter(Seq("2.2.0", "2.2.2")))
    assertEquals(Some("0.0.1"), Version(">=0.0.1 <0.1.0").filter(Seq("0.2.0", "0.0.0", "0.1.0", "0.0.1")))
    assertEquals(Some("2.0.0-beta3"), Version(">= 1.0.0").filter(Seq("2.0.0-beta3")))
  }
}
