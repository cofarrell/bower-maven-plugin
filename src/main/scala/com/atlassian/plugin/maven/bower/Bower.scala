package com.atlassian.plugin.maven.bower

import org.apache.maven.plugin.logging.Log

import java.io.File
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import scala.io.Source

class Bower(outputDirectory: File,
            inputFile: File,
            registryUrl: String,
            registryCloneUrl: String,
            excludes: Array[String],
            log: Log) {

  // The _real_ bower cache doesn't store the Git dirs (WTF?!?)
  val cacheDir = new File(sys.props("user.home"), ".bower-maven/packages")

  def fetchModules(): Unit = resolveAndCopyModules()

  def showLicenses(): Unit = {
    val moduleLines = getModules.map {
      pkg =>
        val licenses = pkg.licenses.map {
          lic => lic.`type` + lic.url.map(" @ " + _).getOrElse("")
        } match {
          case s if s.isEmpty => List("None")
          case s => s
        }
        "%s (%s)".format(pkg.name, licenses.mkString(","))
    }
    val spacer = "-" * 72
    (List(spacer, "LICENSES", spacer) ++ moduleLines ++ List(spacer)).foreach(log.info)
  }

  private def resolveAndCopyModules() = {
    // Make sure the output directory exists before we start forking the world
    outputDirectory.mkdirs()
    getModules.par.foreach {
      pack =>
        log.info("Copying packages from %s into %s".format(pack.name, outputDirectory))
        Copy.copy(
          new File(cacheDir, pack.name),
          new File(outputDirectory, pack.name),
          pack.ignore
        )
    }
  }

  private def loadRegistry = {
    Option(registryCloneUrl) match {
      // This is unique to the maven plugin - bower can only support the HTTP registry
      // HTTP isn't very good with authentication, and cloning with SSH is a good way to ensure each machine
      // has the right access without having to hard-code passwords everywhere
      case Some(cloneUrl) if !cloneUrl.isEmpty =>
        val registryDir = new File(cacheDir, "bower-maven-registry")
        Resolver.cloneOrUpdate(registryDir, cloneUrl, log)
        Registry(Source.fromFile(new File(registryDir, "registry.json")))
      // Otherwise fall back to the normal bower way...
      case _ => Registry(Source.fromURL(registryUrl))
    }
  }

  private def getModules: List[Package] = {
    val registry = loadRegistry
    implicit val ec = ExecutionContext.global

    val resolver = new Resolver(cacheDir, registry, log)

    // Make sure the cache directory exists before we start forking the world
    cacheDir.mkdirs()

    def recursiveResolve(parent: File): Future[List[Package]] = Package.parse(parser(parent)).map {
      pkg => Future.sequence {
        pkg.dependencies
          .filter(dep => !excludes.contains(dep.name))
          .map(dep => Future(resolver.resolve(dep)).flatMap(recursiveResolve))

      }.map(_.flatten).map(pkg :: _)
    }.getOrElse(Future.successful(List(Package(parent.getName))))

    val startDir = if (inputFile.isFile) inputFile.getParentFile else inputFile
    // Remove 'this' package
    Await.result(recursiveResolve(startDir), 5 minutes).tail
  }

  private def parser(parent: File)(name: String) =
    Some(new File(parent, name)).filter(_.isFile).map(Source.fromFile)

}
