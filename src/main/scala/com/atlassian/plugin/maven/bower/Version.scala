package com.atlassian.plugin.maven.bower

sealed trait Version {
  def filter(versions: Seq[String]): Option[String]
}

case object AllVersion extends Version {
  override def filter(versions: Seq[String]) = versions.headOption
}

object ConditionVersion {
  def apply(condition: String, version: String): ConditionVersion = ConditionVersion(condition, SimpleVersion(version))
}

case class ConditionVersion(condition: String, version: SimpleVersion) extends Version {

  override def filter(versions: Seq[String]) = versions.find(satisfies)

  private def satisfies(v2: String): Boolean = condition match {
    case null | "~" => version ~ v2
    case _ if condition.endsWith("=") && version.version == v2 => true
    case _ if condition.startsWith(">") => version > v2
    case _ if condition.startsWith("<") => version < v2
    case _ => false
  }

}

case class ComplexVersion(left: Version, right: Version) extends Version {
  // Filter left last - should be the more significant filter
  override def filter(versions: Seq[String]) =
    right.filter(versions).flatMap(_ => left.filter(versions))

}

case class SimpleVersion(version: String) {

  def ~(right: String) = right.startsWith(version)

  val < = op(_ <= _, _ <= _)

  val > = op(_ >= _, _ >= _)

  def op(p: (Int, Int) => Boolean, ps: (String, String) => Boolean): String => Boolean = {
    right =>
      def asInts(version: String) = version.split("\\.").map(s => try s.toInt catch {
        case e: NumberFormatException => s
      }).toList
      // Make sure not the exact same to ensure < is respected
      version != right && asInts(right).zip(asInts(version)).forall {
        case (l: Int, r: Int) => p(l, r)
        case t => ps(t._1.toString, t._2.toString)
      }
  }

}

object Version {

  val VERSION_WITH_SIGN_REGEX = "(?:(~|=|>=|<|<=)?\\s*((?:\\d|x)+(?:\\.(?:\\d|x)+)?(?:\\.(?:\\d|x)+)?(?:rc\\d+)?)\\s*)"
  val VERSION_REGEX = ("(?:(\\*)|" + VERSION_WITH_SIGN_REGEX + VERSION_WITH_SIGN_REGEX + "?)").r

  // https://github.com/isaacs/node-semver
  def apply(version: String): Version =
    version match {
      case _ if "*" == version => AllVersion
      case VERSION_REGEX(_, leftMod, leftVer, rightMod, rightVer) if rightVer != null => ComplexVersion(ConditionVersion(leftMod, leftVer), ConditionVersion(rightMod, rightVer))
      case VERSION_REGEX(_, modifier, ver, _, _) if ver != null => ConditionVersion(modifier, ver)
      case _ => ConditionVersion("=", version)
    }
}
