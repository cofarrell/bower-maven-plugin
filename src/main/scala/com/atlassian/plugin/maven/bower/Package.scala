package com.atlassian.plugin.maven.bower

import java.util.{List => JList, Map => JMap}
import scala.collection.JavaConverters._
import scala.io.BufferedSource

object Package {

  import com.fasterxml.jackson.databind.ObjectMapper

  def parse(resolver: String => Option[BufferedSource]): Option[Package] = {
    def load(name: String) = resolver(name).map(parseFile)
    // Combine the meta information from bower and NPM, the latter often has licening
    (load("bower.json"), load("package.json")) match {
      case (Some(b), Some(p)) => Some(b |+| b)
      case (b, p) => b.orElse(p)
    }
  }

  private def parseFile(in: BufferedSource): Package = {
    val objectMapper = new ObjectMapper
    val input = objectMapper.readValue(in.bufferedReader(), classOf[JMap[String, Object]]).asScala
    val name = input.get("name").getOrElse("unknown").asInstanceOf[String]
    val deps = input.get("dependencies").map(_.asInstanceOf[JMap[String, String]].asScala).getOrElse(Map()).map {
      case (name, version) => Dep(name, version)
    }.toList
    val ignore = input.get("ignore").map(_.asInstanceOf[JList[String]]).map(_.asScala).getOrElse(Nil)
    val licenses = input.get("licenses").map(_.asInstanceOf[JList[JMap[String, String]]]).map(_.asScala).getOrElse(Nil).map {
      obj => License(
        obj.asScala.get("type").getOrElse("Unknown"),
        obj.asScala.get("url")
      )
    }
    Package(name, deps, licenses, ignore)
  }
}

case class Package(name: String,
                   dependencies: List[Dep] = Nil,
                   licenses: Seq[License] = Nil,
                   ignore: Seq[String] = Nil) {

  // Don't append the sequences - they most likely contain the same information
  // Maybe we should make them sets?
  def |+|(other: Package) = Package(name,
    ifNil(dependencies, other.dependencies),
    ifNil(licenses, other.licenses),
    ifNil(ignore, other.ignore)
  )

  private def ifNil[B <: Seq[_]](a: B, b: B) = if (!a.isEmpty) a else b
}

case class License(`type`: String, url: Option[String] = None)

case class Dep(name: String, version: String)
