package com.atlassian.plugin.maven.bower

import java.io.File
import org.codehaus.plexus.util.{DirectoryScanner, FileUtils}

object Copy {

  def copy(source: File, dest: File, ignore: Seq[String]): Unit = {

    // We can't use DirectoryWalker - not in maven 2.1.0
    val scan = new DirectoryScanner {
      // By default includes take precedence - WTF?!?
      override def couldHoldIncluded(name: String) = false
    }
    scan.setBasedir(source)
    scan.setExcludes((ignore ++ Seq(".*")).toArray)
    scan.scan()
    scan.getIncludedFiles.foreach {
      file => copyIfModified(new File(source, file), new File(dest, file))
    }
    copyIfModified(new File(source, ".bower.json"), new File(dest, ".bower.json"))
  }

  def copyIfModified(s: File, d: File) =
    if (d.lastModified() < s.lastModified()) {
      FileUtils.copyFile(s, d)
    }
}
