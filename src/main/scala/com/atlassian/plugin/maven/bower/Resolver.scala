package com.atlassian.plugin.maven.bower

import org.apache.maven.plugin.logging.Log
import org.codehaus.plexus.util.FileUtils
import java.io.File
import scala.sys.process._
import scala.util.parsing.json.JSONObject

class Resolver(dir: File, registry: Registry, log: Log) {

  val progress = if (log.isDebugEnabled) "--progress" else ""
  val quietLogger = ProcessLogger(_ => ())

  def resolve(dep: Dep) = {

    val cwd = new File(dir, dep.name)

    def defaultExec(logger: ProcessLogger = null): ProcessBuilder => Unit = {
      process =>
        val exit = if (logger == null) process.! else process.!(logger)
        if (exit > 0) {
          sys.error("Error running: " + process)
        }
    }

    def run[T](cmd: String, exec: ProcessBuilder => T = defaultExec(), cwd: File = cwd) =
      exec(Process(cmd, cwd = cwd))

    def matchVersion = Version(dep.version)
      .filter(run("git tag", _.lines))
      // Check if the version is a sha to avoid an extra fetch
      .orElse(Some(dep.version).filter(sha => run("git rev-parse " + sha, _.!(quietLogger) == 0)))

    def checkout() = matchVersion.map {
      version =>
        run("git checkout %s".format(version), defaultExec(quietLogger))
        registry(dep.name).foreach {
          url => writeMetadata(cwd, dep)
        }
    }.orElse(sys.error("Invalid version '%s' for %s".format(dep.version, cwd)))

    if (!cwd.exists()) {
      registry(dep.name) match {
        case Some(cloneUrl) =>
          run("git clone %s %s %s".format(progress, cloneUrl, cwd), cwd = cwd.getParentFile)
          checkout()
        case None => sys.error("Not found: " + dep.name)
      }
    } else {
      matchVersion match {
        case Some(version) => checkout()
        case None =>
          run("git fetch %s".format(progress))
          checkout()
      }
    }
    cwd
  }

  def writeMetadata(depDir: File, dep: Dep): Unit = {
    val orig = new File(depDir, "bower.json")
    val file = new File(depDir, ".bower.json")
    if (orig.exists()) FileUtils.copyFile(orig, file)
    else {
      // There is more metadata, but this is all you _need_ to work with bower (so that it doesn't show 'missing')
      val json = JSONObject(Map(
        "name" -> dep.name,
        "version" -> dep.version
      ))
      FileUtils.fileWrite(file.getAbsolutePath, json.toString())
    }
  }
}

object Resolver {

  def cloneOrUpdate(dest: File, cloneUrl: String, log: Log) = {
    val progress = if (log.isDebugEnabled) "--progress" else ""
    if (dest.exists()) {
      log.info("Updating Bower module '%s' from '%s'".format(dest.getName, cloneUrl))
      // Make sure we pull (and not fetch - we want to be on the latest)
      Process("git fetch %s".format(progress), cwd = dest).!
      Process("git reset --hard origin/master", cwd = dest).!(ProcessLogger(_ => ()))
    }
    else {
      log.info("Cloning Bower module '%s' from '%s'".format(dest.getName, cloneUrl))
      Process("git clone %s %s %s".format(progress, cloneUrl, dest)).!
    }
  }
}
