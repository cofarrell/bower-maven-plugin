package com.atlassian.plugin.maven.bower

import scala.io.Source
import scala.util.parsing.json.{JSONObject, JSONArray, JSON}

object Registry {

  def apply(source: Source): Registry =
    JSON.parseRaw(source.getLines().mkString("")) match {
      case Some(JSONArray(array)) => new Registry(array.collect {
        case JSONObject(map) => map("name").asInstanceOf[String] -> map("url").asInstanceOf[String]
      }.toMap)
      case _ => new Registry(Map())
    }
}

case class Registry(registry: Map[String, String]) {

  def apply(name: String) = registry.get(name)
}
