package com.atlassian.plugin.maven.bower;

import com.atlassian.plugin.maven.bower.Bower;
import org.apache.maven.plugin.AbstractMojo;

import java.io.File;

public abstract class AbstractBowerMojo extends AbstractMojo {

    /**
     * Where the resulting files will be downloaded.
     *
     * @parameter expression="${recess.outputDirectory}" default-value="${basedir}/src/main/resources/META-INF"
     */
    private File outputDirectory;

    /**
     * Package file which declares the dependencies.
     *
     * @parameter expression="${recess.inputFile}" default-value="${basedir}/bower.json"
     */
    private File inputFile;

    /**
     * Override URL to download packages from.
     *
     * @parameter expression="${recess.registryUrl}" default-value="http://bower.herokuapp.com/packages"
     */
    private String registryUrl;

    /**
     * Clone URL to download a Git repository from that contains a 'registry.json' file.
     *
     * @parameter expression="${recess.registryCloneUrl}"
     */
    private String registryCloneUrl;

    /**
     * Packages to exclude.
     *
     * @parameter
     */
    private String[] excludes = new String[0];

    protected Bower getBower() {
        return new Bower(outputDirectory, inputFile, registryUrl, registryCloneUrl, excludes, getLog());
    }
}
