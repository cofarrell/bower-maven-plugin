package com.atlassian.plugin.maven.bower;

import org.apache.maven.plugin.MojoExecutionException;

/**
 * Goal that offers Recess support in Maven builds.
 *
 * @goal fetch-modules
 * @phase generate-sources
 */
public class BowerFetchModulesMojo extends AbstractBowerMojo {

    public void execute() throws MojoExecutionException {
        getBower().fetchModules();
    }

}
