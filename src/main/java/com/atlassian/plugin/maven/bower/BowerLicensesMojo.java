package com.atlassian.plugin.maven.bower;

import org.apache.maven.plugin.MojoExecutionException;

/**
 * Goal that offers Recess support in Maven builds.
 *
 * @goal licenses
 */
public class BowerLicensesMojo extends AbstractBowerMojo {

    public void execute() throws MojoExecutionException {
        getBower().showLicenses();
    }
}
